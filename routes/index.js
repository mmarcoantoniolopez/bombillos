const { Router } = require('express');
const router = Router();
var bodyParser = require('body-parser')
var fs = require('fs');

// create application/json parser
var jsonParser = bodyParser.json()

const valuePoint= (array, x, y)=>{
    let hi=valueHI(array, x, y);
    let hd= valueHD(array, x, y);
    let ve=valueVE(array, x, y);
    let va=valueVA(array, x, y);;
    return [y, x, hi, hd, ve, va, hi + hd + ve + va];
}

const valueHD =(array, x, y)=>{
    let count =0;
    for(let i= x; i<array[y].length; i++){
        if(array[y][i]==0) count++
        else return count;
    }
    return count;
}

const valueHI =(array, x, y)=>{
    let count =0;
    for(let i = x; i>=0; i--){
        if(array[y][i]==0) count++
        else return count;
    }
    return count;
}

const valueVE =(array, x, y)=>{
    let count =0;
    for(let i = y; i>=0; i--){
        if(array[i][x]==0) count++
        else return count;
    }
    return count;
}

const valueVA =(array, x, y)=>{
    let count =0;
    for(let i= y; i<array.length; i++){
        if(array[i][x]==0) count++
        else return count;
    }
    return count;
}

const solutionSort = (items)=>{
    items.sort(function (a, b) {
        if (a.lightPoints > b.lightPoints) {
            return 1;
        }
        if (a.lightPoints < b.lightPoints) {
            return -1;
        }
        return 0;
    });
}

const itemsSort = (items)=>{
    items.sort(function (a, b) {
        if (a.value > b.value) {
            return -1;
        }
        if (a.value < b.value) {
            return 1;
        }
        return 0;
    });
}

setLightBulbScenaries = (array, y, x) =>{
    let complet=false;
    lightPoints =[]
    lightPoints.push({y, x})
    var lightBulb = 0;
    matrix = setLightBulb(array, y, x)
    while(!complet){
        let dataPointNext = getBestPoint(matrix);
        if(dataPointNext.length == 0) break;
        matrix = setLightBulb(matrix, dataPointNext[0].y, dataPointNext[0].x)
        lightPoints.push({y:dataPointNext[0].y, x: dataPointNext[0].x})
        complet = dataPointNext.length == 0
    }
    
    return {matrix, numberLightBulb: lightBulb, lightPoints};
}

setLightBulb = (array, y, x) =>{
        // iluminando derecha 
    array[y][x]='x'
    for(let i= x; i<array[y].length; i++){
        if(array[y][i]==0) array[y][i]='i'        
        if(array[y][i]==1) break;
    }
    //iluminando izquierda
    for(let i = x; i>=0; i--){
        if(array[y][i]==0) array[y][i]='i'
        if(array[y][i]==1) break;
    }
    //iluminando arriba
    for(let i = y; i>=0; i--){
        if(array[i][x]==0) array[i][x]='i'
        if(array[i][x]==1) break;
    }
    //iluminando abajo
    for(let i= y; i<array.length; i++){
        if(array[i][x]==0) array[i][x]='i'
        if(array[i][x]==1) break;
    }
    return array
}

getBestPoint = (matrix) =>{
    var items =[]
    for(let y=0; y<matrix.length; y++){
        for(let x=0; x<matrix[y].length; x++){
            if(matrix[y][x]==0) {
                const dataPointEvaluation = valuePoint(matrix, x, y)
                items.push({y, x,  value: dataPointEvaluation[6]})
                //console.log(valuePoint(matrix, x, y))
            }
        }
    }
    itemsSort(items);
    return items
}
printResult = (matrixOriginal, matrixSolution, numLightBulb) =>{
    console.log('Matriz Original:' )
    let line =''
    let lineSolution = ''
    for(let y=0; y<matrixOriginal.length; y++){
        line =''
        for(let x=0; x<matrixOriginal[y].length; x++){
            line += matrixOriginal[y][x]+' '
            //console.log('inside', line)
        }
        //console.log(matrixOriginal[y], matrixOriginal[y].length)
        console.log(line.trim())
    }
    console.log('MEJOR SOLUCIÓN' )
    console.log('Total de bombillos usados:', numLightBulb)
    console.log('Matriz solución iluminación:' )
    for(let y=0; y<matrixSolution.length; y++){
        lineSolution =''
        for(let x=0; x<matrixSolution[y].length; x++){
            if(matrixSolution[y][x] == 'i') matrixSolution[y][x] = 0
            lineSolution += matrixSolution[y][x]+' '
        }
        console.log(lineSolution.trim())
    }
}

copyMatrix = (matrix)=>{
    let matrixData = []
    for(let y=0; y<matrix.length; y++){
        let row = []
        for(let x=0; x<matrix[y].length; x++){
            row.push(matrix[y][x])
        }
        matrixData.push(row);
    }
    return matrixData;
}
router.post('/lighting', async (req, res) => {
    let TRY_NUMBER = 0
    let resultScenaries = []
    let multiScenaries =[]
    try {  
        var data = fs.readFileSync('files/room.txt', 'utf8');
        let row_list = data.split('\n');   
        var matrix = row_list.map((r) => r.split(' '));
        var matrixOriginal = row_list.map((r) => r.split(' '));

        const pointBest = getBestPoint(matrix);
        TRY_NUMBER = pointBest.length / 2
        for(let z=0; z<TRY_NUMBER; z++){
            matrix = copyMatrix(matrixOriginal)
            resultScenaries = setLightBulbScenaries(matrix, pointBest[z].y, pointBest[z].x)
            multiScenaries.push({ matrix: copyMatrix(resultScenaries.matrix), lightPoints:  resultScenaries.lightPoints.length })
            //printResult(matrixOriginal, resultScenaries.matrix, resultScenaries.lightPoints.length)
        }
        solutionSort(multiScenaries)
        printResult(matrixOriginal, multiScenaries[0].matrix, multiScenaries[0].lightPoints)
        //console.log('multiScenaries=>', multiScenaries)

    } catch(e) {
        console.log('Error:', e.stack);
    }

    res.status(200).send({
        Mensaje: 'Proceso concluido con éxito.',
        Error:'',
        MultiplesScenariosEjecutados:multiScenaries
    });
});
module.exports = router;