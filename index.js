const express = require('express');
const PORT= 3000
const app = express();

// routes 
app.use(require('./routes/index'))

app.listen(PORT, ()=>{
    console.log(`El servidor está corriendo en http://localhost:${PORT}`)
})