# bombillos
Algoritmos de optimización para iluminar una habitación
# Ejecución
1.- Colocar el archivo txt(o sustituirlo) y nombrarlo "room.txt" de matriz en la ruta files/room.txt con 1 como espacios con paredes y ceros como espacios sin pared, los valores de la matriz DEBEN estar separado por un solo espacio entre si.

2.- Instalar packages con npm install.

3.- Levantar el API con " npm run develop "

4.- Consumir desde un servicio como Postman la siguiente url  POST "localhost:3000/lighting".

5.- El resultados se imprime en la terminal. En la matriz se vera el valor "x" que significa la instalacion de una bombilla, y tambien se impreme el total optimo instalada.